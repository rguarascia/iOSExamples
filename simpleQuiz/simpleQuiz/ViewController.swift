//
//  Ryan Guarascia
//
//  ViewController.swift
//  simpleQuiz
//
//  Created by student on 2018-09-05.
//  Copyright © 2018 Mohawk College. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet var questionLabel: UILabel!
    @IBOutlet var answerLabel: UILabel!
    
    let questions: [String] = [
        "What is the best car brand?", "What is the best engine made?", "Does gas mileage matter?"
    ]
    let answers: [String] = [
        "Dodge", "Hemi", "No. Horsepower > gas mileage"
    ]
    
    var qIndex: Int = 0
    
    @IBAction func showNextQuestion(_ sender : UIButton) {
        qIndex += 1
        if(qIndex == questions.count) {
            qIndex = 0
        }
        
        let question: String = questions[qIndex]
        
        questionLabel.text = question
        answerLabel.text = "???"
    }
    
    @IBAction func showAnswer(_ sender : UIButton) {
        let answer: String = answers[qIndex]
        
        answerLabel.text = answer
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        questionLabel.text = questions[qIndex]
    }
    

}

