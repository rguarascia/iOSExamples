//: Playground - noun: a place where people can play

import Cocoa

var str = "Hello, playground"
str = "Hello, Swift."

//Let is used to create constants
let constStr = str

//constStr = "This will error out"

let nextYear: Int
let bodyTemp: Float
let hasPet: Bool

//var arrayOfInts: Array<Int> long version

//short hand
var arrayOfInts: [Int]

var dicOfCaps: Dictionary<String, String>

var lotteryNums: Set<Int>.ArrayLiteralElement

//literal
let number = 42
let fmStaton =  88.1

var countingUp = ["One", "Two"]
let nameByParkingSpace = [13: "Alice", 27: "Bob"]

let secondElement = countingUp[1] //Two

let emptyString = String()
let emptyArrayOfInts = [Int]()
let emptySetOfFloats = Set<Float>()

let defaultNumber = Int()
let defaultBool = Bool()

let meaningOfLife = String(number)

let availableRooms = Set([205, 411, 412])

let defaultFloat = Float()
let floatFromLiteral = Float(3.14)

let easyPi = 3.14

let floatFromDouble = Float(easyPi)

let floatingPi: Float = 3.14

countingUp.count
emptyString.isEmpty

countingUp.append("three")

//Optionals
var anOptionalFloat: Float?
var anOptionalArrayOfStrings: [String]?
var anOptionalArrayOfOptionalStrings: [String?]

var reading1: Float?
var reading2: Float?
var reading3: Float?

reading1 = 9.8
reading2 = 9.2
reading3 = 9.7

//Forcibly unwrap
//let avgReading = (reading1! + reading2! + reading3!) / 3

if let r1 = reading1,
    let r2 = reading2,
    let r3 = reading3 {
    _ = (r1 + r2 + r3) / 3
} else {
    _ = "Instrument reported a reading that was nil."
}

//Loooooops

//For w/ Range
let range = 0..<countingUp.count
for i in range {
    let string = countingUp[i]
}

//Foreach
for string in countingUp {
}

//Foreach w/ index (Like PHP =>)
//for (i, string) in countingUp.enumerate() {
    // (0, "one"), (1, "two")
//}

//Tuple
for (space, name) in nameByParkingSpace {
    let permit = "Space \(space): \(name)"
}


//Enums and Switch

//Defines the pies
enum PieType {
    case Apple
    case Cherry
    case Pecan
    case CoconutCreamPie
}
//Sets a constat of my fav pie
let favouritePie = PieType.CoconutCreamPie

//Switches uses  the ranges
let osxVersion: Int = 11
switch osxVersion {
case 0...8:
    print("A big cat")
case 9:
    print("Mavericks")
case 10:
    print("Yosemite")
case 11:
    print("El Capitan")
default:
    print("Greetings, people of the future! What's new in 10.\(osxVersion)?")
}

//set a key to each val
/*
enum PieType: Int {
    case Apple = 0
    case Cherry = 1
    case Pecan = 2
    case CoconutCreamPie = 3
} */
