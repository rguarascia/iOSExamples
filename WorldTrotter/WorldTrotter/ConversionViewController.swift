//
//  ConversionViewController.swift
//  WorldTrotter
//
//  Created by student on 2018-09-17.
//  Copyright © 2018 Mohawk College. All rights reserved.
//

import UIKit

class ConversionViewController: UIViewController, UITextFieldDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("ConversionViewController")
        
        updateCelsius()
    }
    
    @IBOutlet var celsiusLabel : UILabel!
    @IBOutlet var textField: UITextField!
    
    var fahrenheitVal: Measurement<UnitTemperature>? {
        didSet {
            updateCelsius()
        }
    }
    
    var celsiusVal: Measurement<UnitTemperature>? {
        if let fahrenheitVal = fahrenheitVal {
            return fahrenheitVal.converted(to: .celsius)
        } else {
            return nil
        }
    }
    
    let numberFormatter: NumberFormatter = {
        let nf = NumberFormatter()
            nf.numberStyle = .decimal
            nf.minimumFractionDigits = 0
            nf.maximumFractionDigits = 1
        return nf
    }()
    
    @IBAction func fahremheitFieldEditingChanged(textField: UITextField)  {
        if let text = textField.text, let value = Double(text) {
            fahrenheitVal = Measurement(value: value, unit: .fahrenheit)
        } else {
            fahrenheitVal = nil
        }
    }
    
    @IBAction func dissmissKeyboard(_ sender: UITapGestureRecognizer) {
        textField.resignFirstResponder()
    }
    
    func updateCelsius() {
        if let celsiusVal = celsiusVal {
            celsiusLabel.text = numberFormatter.string(from: NSNumber(value: celsiusVal.value))
        } else {
            celsiusLabel.text = "Empty..."
        }
    }
    
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        
        let exisitingTextDec = textField.text?.range(of: ".")
        let replacementTextDec = string.range(of: ".")
        
        if exisitingTextDec != nil,
            replacementTextDec != nil {
            return false
        } else {
            return true
        }
    }
}
